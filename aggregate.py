
from __future__ import print_function
import os
import sys
import numpy as np
import argparse
import matplotlib.pyplot as plt
import csv
import subprocess

class aggregateData(object):

    def __init__(self, showplots=False):
        
        self.showplots     = showplots # Show figures if True
        self.period_in     = None      # Injected period
        self.dm_in         = None      # Injected DM
        self.width_in      = None      # Injected width (turns)
        self.width_in_s    = None      # Injected width (s)
        self.sn_in         = None      # Injected signal-to-noise ratio
        self.flux_in       = None      # Injected flux density
        self.nbins         = None      # Number of bins in folded profile
        self.period        = None      # Recovered period
        self.pdot          = None      # Recovered spin-down
        self.width         = None      # Recovered width (s)
        self.dm            = None      # Recovered DM
        self.sn            = None      # Recovered signal-to-noise ratio
        self.shape         = None      # Injected pulse shape

    @staticmethod
    def load_profile_data(self, data):
        pass

    @staticmethod
    def get_fil_list():
        here = os.getcwd()
        idents = []
        for this_file in os.listdir(here):
            if this_file.endswith(".fil"):
                ident = os.path.splitext(this_file)[0]
                idents.append(ident)

        return idents

    def set_opts(self, ident): 
        optname = ident + ".opt"
        return optname

    def set_txts(self, ident):
        txtname = ident + ".txt"
        return txtname

    def check_file(self, this_file):
        if not os.path.exists(this_file):
            return False
        return True

    def plot(self, bins, power, name):
        pngname = name + ".png"
        plt.plot(bins, power, 'k-')
        plt.xlabel("Bin", fontsize=15)
        plt.xlabel("Power", fontsize=15)
        plt.title(name)
        plt.tight_layout()
        plt.savefig(pngname, format='png', dpi=400)
        if self.showplots:
            print("Program will continue when plot is closed")
            plt.show()
        plt.close()

    def get_period_in(self, ident):
        period_in = float(ident.split('_')[2]) / 1e3
        return period_in

    def get_dm_in(self, ident):
        dm_in = float(ident.split('_')[4])
        return dm_in

    def get_width_in(self, ident):
        width_in = float(ident.split('_')[3])
        return width_in

    def get_sn_in(self, ident):
        sn_in = float(ident.split('_')[7])
        return sn_in

    def get_flux_in(self, ident):
        flux_in = float(ident.split('_')[8])
        return flux_in

    def get_shape(self, ident):
        shape = ident.split('_')[6]
        return shape

    def process_txt(self, txt):
        with open(txt, 'r') as this_txt:
                for line in this_txt.readlines():
                    fields = line.split()
                    if fields:
                        if "Period" in fields[0]:
                            period = fields[3]
                        if "Pdot" in fields[0]:
                            pdot = fields[3]
                        if "Width" in fields[0]:
                            width = fields[3]
                        if "DM" in fields[0]:
                            dm = fields[3]
                        if "S/N" in fields[0]:
                            sn = fields[3]
        return period, pdot, width, dm, sn

    def write_csv_header(self):
        line = ['Filename', 'Shape', 
                'Period in', 'Period out',
                'DM in', 'DM out',
                'Flux in', 'SN in', 'SN out', 'SN out / SN in',
                'Width in (rots)', 'Width in (s)', 'Width out (s)',
                'Width out / Width in',
                'Pdot out', 'nbins']
                
        with open('results.csv', 'w') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerow(line)

        csvFile.close()
            
    def write_data(self, ident):
        line = [ident, self.shape,
                self.period_in, float(self.period) * 1e-3,
                self.dm_in, self.dm,
                self.flux_in, self.sn_in, self.sn, (float(self.sn) / float(self.sn_in)),
                self.width_in, self.width_in_s, self.width,
                float(self.width) / float(self.width_in_s),
                self.pdot, self.nbins]

        with open('results.csv', 'a') as csvFile:
            writer = csv.writer(csvFile)
            writer.writerow(line)

        csvFile.close()

    def make_montage(self):
        command = ['montage', '-tile', '1x1', 
                   '-geometry', '+4+4', 
                   '*.png', 'profiles.pdf']

        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        proc.communicate()
        proc.wait()

      
    def processResults(self):

        # Get list of filterbank files from pwd
        idents = self.get_fil_list()

        # Prepare csv file
        self.write_csv_header()

        for ident in idents:

            # Unset parameters to avoid overwrite
            self.period_in = None
            self.dm_in = None
            self.width_in = None
            self.width_in_s = None
            self.sn_in = None
            self.flux_in = None
            self.nbins = None
            self.period = None
            self.pdot = None
            self.width = None
            self.dm = None
            self.sn = None
            self.shape = None

            # Get pulse shape from ident
            self.shape = self.get_shape(ident)
 
            # Get input period from ident
            self.period_in = self.get_period_in(ident)

            # Get input dm from ident
            self.dm_in = self.get_dm_in(ident)

            # get input width from ident
            self.width_in = self.get_width_in(ident)

            # Get input width in seconds
            self.width_in_s = self.width_in * self.period_in

            # Get input signal-to-noise from ident
            self.sn_in = self.get_sn_in(ident)

            # Get input flux from ident
            self.flux_in = self.get_flux_in(ident)

            # Get opt filenames
            opt = self.set_opts(ident)

            # Check the optfile exists:
            if self.check_file(opt):
                
                # Load opt file
                bins, power = np.loadtxt(opt, unpack=True)
           
                # Plot opt file to png
                self.plot(bins, power, opt)

                # Get number of bins
                self.nbins = len(power)

            else:
                self.nbins = None

            # Get txt filenames
            txt = self.set_txts(ident) 

            # Check the txt file exists:
            if self.check_file(txt):
                self.process_txt(txt)

                # Grab the parameters from the txt file
                self.period, self.pdot, self.width, self.dm, self.sn = self.process_txt(txt)

            self.write_data(ident)

        # Make pdf of profiles
        self.make_montage()

def main():

    '''
    Processes command line arguments and 
    passes them to the class
    '''

    parser = argparse.ArgumentParser(description='Test vector results explorer')
    parser.add_argument('-s','--showplots', help='Show figures', required=False, action='store_true')
    args = parser.parse_args()
    
    aggregator = aggregateData(args.showplots)
    aggregator.processResults()


if __name__ == '__main__':
    main()
