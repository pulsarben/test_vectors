
import os
import pickle
import pytest
import numpy as np
import sys
from lmfit.models import GaussianModel
from GenerateGaussian import MakeGaussianProfile

dc = 0.1
nbins = 256

DATA_DIR = os.path.dirname(__file__) + "/fixtures"

def create_obj():
    profile = MakeGaussianProfile(nbins=nbins, dc=dc, show=True, norm=True) 
    return profile

def test_fhwp_conv():
    expected_result = 25.6
    assert create_obj().dc_to_fwhp_bins(nbins, dc) == expected_result

def test_sigma_conv():
    expected_result = 10.871319043686645
    assert create_obj().fwhp_to_sigma(25.6) == expected_result

def test_dc_good():
    assert create_obj().check_dc_val(0.1) == True

def test_dc_bad():
    assert create_obj().check_dc_val(1.5) == False

def test_fit_fwhp():
    powerfile = DATA_DIR + "/test_Gaussian_DC=0.1_BINS=256_FWHM_25.6.asc"
    power = np.loadtxt(powerfile, unpack=True)
    x = np.linspace(0, nbins -1, nbins)
    fit_output = create_obj().do_least_sq(power, x)
    expect_fwhp = 25.6
    actual_fwhp = round(fit_output.params['fwhm'].value, 1)
    assert expect_fwhp == actual_fwhp

def test_fit_sigma():
    powerfile = DATA_DIR + "/test_Gaussian_DC=0.1_BINS=256_FWHM_25.6.asc"
    power = np.loadtxt(powerfile, unpack=True)
    x = np.linspace(0, nbins -1, nbins)
    fit_output = create_obj().do_least_sq(power, x)
    expect_sigma = 10.87
    actual_sigma = round(fit_output.params['sigma'].value, 2)
    assert expect_sigma == actual_sigma
 
def test_fit_mean():
    powerfile = DATA_DIR + "/test_Gaussian_DC=0.1_BINS=256_FWHM_25.6.asc"
    power = np.loadtxt(powerfile, unpack=True)
    x = np.linspace(0, nbins -1, nbins)
    fit_output = create_obj().do_least_sq(power, x)
    expect_mean = 128.0
    actual_mean = round(fit_output.params['center'].value, 1)
    assert expect_mean == actual_mean 

def test_fit_height():
    powerfile = DATA_DIR + "/test_Gaussian_DC=0.1_BINS=256_FWHM_25.6.asc"
    power = np.loadtxt(powerfile, unpack=True)
    x = np.linspace(0, nbins -1, nbins)
    fit_output = create_obj().do_least_sq(power, x)
    expect_height = 1.0
    actual_height = round(fit_output.params['height'].value, 1)
    assert expect_height == actual_height
