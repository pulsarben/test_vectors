
from __future__ import print_function
import numpy as np
import sys, os
import argparse
import yaml
import subprocess
import glob
from GenerateNoise import NoiseGenerate
from GenerateGaussian import MakeGaussianProfile
from GenerateAcceleratedVectors import BatchGenerate


class GeneratePipeline(object):
    
    def __init__(self, mode, yaml_dir, output ):

        self.mode       = mode     # keyword for which sub-set of vectors to create
        self.output     = output   # path to directory to store the output files
        self.yaml_dir   = yaml_dir

    def run_process(self, command):

        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        proc.communicate()
        proc.wait()

    def processing_steps(self):

        # Check everything is in place to process:
        yamlfile =  str(self.yaml_dir) + "/" + str(self.mode) + ".yaml"
        yaml_exists = os.path.isfile(yamlfile)
        if yaml_exists:
            print("Using yaml file:", yamlfile)
        else:
            print("Error: Requested mode does not have an associated yaml file:", yamlfile)
            exit()

        if os.path.isdir(self.output):
            print("Writing files to", self.output)
        else:
            os.makedirs(self.output)
            print("Requested output directory does not yet exist, creating it:", self.output)
#            exit()

        # Read in values from yaml file
        with open(yamlfile) as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            
        for key, values in data.items():
            
            # Generate noise file
            # NoiseGenerate(tobs, tsamp, mjd, fch1, chbw, nbits, nchans, name, output, seed=False) 
            if key == 'noise': 
                noise_args = values
                noise_command = ['GenerateNoise.py', noise_args] 
                print("Pipeline running: ", ' '.join(map(str, noise_command))) 
                self.run_process(noise_command) 
                
            # Generate Gaussian profiles
            # MakeGaussianProfile(nbins, dc, show = False, norm = False)        
            if key == 'profiles':
                widths = values
                
                if (os.path.isdir("./profiles")):
                    print("Profile directory already exists, adding new profiles to same directory!")
                else:
                    os.makedirs("profiles")
                    print("Pipeline: Making profile directory")
                    
                for w in widths:
                    profile_command = ['GenerateGaussian.py',\
                                       '-n', '128',\
                                       '-p',\
                                       '-d', w]
                    print("Pipeline running: ", ' '.join(map(str, profile_command))) 
                    self.run_process(profile_command) 

                asc_files = glob.glob('Gaussian*.asc')
                for this_file in asc_files:
                    move_command = ['mv', this_file, './profiles/']
                    print("Pipeline running: ", ' '.join(map(str, move_command)))
                    self.run_process(move_command)
                        
            # Generate test vectors
            # BatchGenerate(noise, profiles, dm, freqs, accn, sn, path, boxcar=False, nogen=False, smear=False)
            if key == 'vectors':
                for vector_id in data['vectors']:
                    for this_dm in values[vector_id]['dm']:
                        for this_freq in values[vector_id]['freq']:
                            for this_acc in values[vector_id]['acc']:
                                for this_snr in values[vector_id]['snr']:
                                    vector_command = ['GenerateAcceleratedVectors.py',\
                                                      '-n', 'noise.fil',\
                                                      '-p', './profiles/',\
                                                      '-d', this_dm,\
                                                      '-f', this_freq,\
                                                      '-a', this_acc,\
                                                      '-s', this_snr,\
                                                      '-l', self.output]
                                    print("Pipeline running: ", ' '.join(map(str, vector_command)))
                                    self.run_process(vector_command)

        # Move the noise file into the output directory
        move_command = ['mv', 'noise.fil', self.output]
        print("Pipeline running: ", ' '.join(map(str, move_command)))
        self.run_process(move_command)
        
        # Move the profile files from ./profiles/ into the output directory
        asc_files = glob.glob('./profiles/Gaussian*.asc')
        for this_file in asc_files:
            move_command = ['mv', this_file, self.output]
            print("Pipeline running: ", ' '.join(map(str, move_command)))
            self.run_process(move_command)
        # Clean up
        rm_command = ['rmdir', "./profiles"]
        self.run_process(rm_command)

        # Done!
        print("Test vectors generated and stored in", self.output)
        
        return None


def main():


    # Process command line args
    parser = argparse.ArgumentParser(description='Test vector generation pipeline')
    parser.add_argument('-m', '--mode', help='Keyword for which sub-set of vectors to create (DDTR, FDAS-FOP, FDAS-SLOW, FDAS-PER, FDAS-ACCN, FLDO)', required=True, type=str)
    parser.add_argument('-d', '--yamldir', help='Path to directory containing yaml file(s)', required=True, type=str)
    parser.add_argument('-o', '--output', help='Path to output directory', required=True, type=str)
    args = parser.parse_args()

    pipeline = GeneratePipeline(args.mode, args.yamldir, args.output)
    pipeline.processing_steps()

if __name__ == '__main__':
    main()

