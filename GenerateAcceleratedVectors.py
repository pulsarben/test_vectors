
from __future__ import print_function
import numpy as np
import sys, os
import argparse
import subprocess
from fluxCalc import GetFlux

class BatchGenerate(object):

    def __init__(self, noise, profiles, dm, freqs, accn, sn, path, boxcar=False, nogen=False, smear=False):

        self.noise           = noise    # Path to noise file
        self.profiles        = profiles # Path to profiles directory
        self.dm              = dm       # List of DMs
        self.freqs           = freqs    # List of spin-frequencies
        self.accn            = accn     # List of accelerations
        self.sn              = sn       # Required signal-to-noise
        self.tsys            = 25.0     # System temperature
        self.gain            = 1.0      # Gain
        self.bw              = 320e6    # Bandwidth 
        self.tobs            = 600      # Observation length (s)
        self.npol            = 1        # Number of polarisations
        self.path            = path     # Output directory
        self.boxcar          = boxcar   # Pulse shape
        self.nogen           = nogen    # Just show commands - no generation
        self.smear           = smear    # Intra-channel DM smearing is off

    @staticmethod
    def prof_list(profiles):
        '''
        Looks in the profiles directory and 
        forms a list of profile files
        '''
        list_of_profs = os.listdir(profiles)
        return list_of_profs

    @staticmethod
    def get_dc_from_prof(prof):
        '''
        Uses the profile filename to extract
        the duty cycle
        '''
        segments = prof.split('_')
        field = segments[1]
        dc = float(field.split('=')[1])
        return dc

    def name_outfile(self):
        '''
        Constructs the name of the filtebank file to be
        produced
        '''
        period_ms = (1.0 / self.this_freq) / 1e-3
        if self.boxcar:
            outfile = "Default_1_" + str(period_ms) + "_" + str(self.dc) + "_" + str(self.this_dm) + "_" + str(self.this_accn) + "_Square_" + str(self.sn) + "_" + str(self.flux) + ".fil"
        else:
            outfile = "Default_1_" + str(period_ms) + "_" + str(self.dc) + "_" + str(self.this_dm) + "_" + str(self.this_accn) + "_Gaussian_" + str(self.sn) + "_" + str(self.flux) + ".fil"

        #print(outfile)
        return outfile

    def form_command(self):
        '''
        Sets up the call to ft_inject_pulsar
        '''
        prof = self.profiles + "/" + self.prof
        outpath = self.path + "/" + self.this_outfile
        if not self.smear:
            command = ['ft_inject_pulsar', 
                        self.noise, 
                        '-p', prof, 
                        '-S', str(self.flux), 
                        '--f0', str(self.this_freq), 
                        '-D', str(self.this_dm),
                        '--accn', str(self.this_accn),
                        '--pepoch', '56000.0', 
                        '--tsys', str(self.tsys), 
                        '--gain', str(self.gain), 
                        '--npol', str(self.npol), 
                        '-x',
                        '-o', outpath]
        else: 
            command = ['ft_inject_pulsar',
                        self.noise,
                        '-p', prof,
                        '-S', str(self.flux),
                        '--f0', str(self.this_freq),
                        '-D', str(self.this_dm),
                        '--accn', str(self.this_accn),
                        '--pepoch', '56000.0',
                        '--tsys', str(self.tsys),
                        '--gain', str(self.gain),
                        '--npol', str(self.npol),
                        '-o', outpath] 
        #command = ' '.join(map(str, command_list))
        return command

    def run_injection(self, command):
        '''
        TO DO: implement
        '''
        #print(command)
        proc = subprocess.Popen(command, stdout=subprocess.PIPE)
        proc.communicate()
        proc.wait()

    def make_batch(self):
        '''
        The main method that does everything
        '''
        if os.path.isdir(self.path):
            pass
        else:
            raise OSError("No such directory: {}".format(self.path))

        # Get list of profiles
        profs = self.prof_list(self.profiles)
       
        # Go through each profile 
        for prof in profs:
            self.prof = prof
            # Get duty cycle from profile filename
            self.dc = self.get_dc_from_prof(self.prof)

            # Go through each DM
            for this_dm in self.dm:
                self.this_dm = this_dm

                # Go through each frequency
                for this_freq in self.freqs:
                    self.this_freq = float(this_freq)

                    # Go through each acceleration
                    for this_accn in self.accn: 
                        self.this_accn = float(this_accn)
                        
                        # Get flux density for input signal-to-noise
                        call_flux = GetFlux(self.sn, self.bw, self.tsys, self.gain, self.tobs, self.npol, self.dc, 1.0 / self.this_freq)
                        self.flux = call_flux.flux()
                        #print(self.prof, self.dc, self.this_dm, self.this_freq, flux, self.sn)
                        
                        # Set name of output filterbank file
                        self.this_outfile = self.name_outfile()  
                        # Generate command

                        self.com = self.form_command()

                        # Call ft_inject_pulsar
                        running_now = ' '.join(map(str, self.com)) 
                        print("Running:\n {}".format(running_now))
                        if not self.nogen:
                            self.run_injection(self.com)
                            print("\nInjection complete!")

        return None
                     
def main(argv=None):

    '''
    Processes command line arguments and 
    passes them to the class
    '''

    parser = argparse.ArgumentParser(description='Test vector batch generator')
    parser.add_argument('-n','--noise', help='Path to noise file', required=True, type=str)
    parser.add_argument('-p','--profiles', help='Path to directory containing profiles', required=True, type=str)
    parser.add_argument('-d','--dm', help='Dispersion Measure list', required=True, nargs='+')
    parser.add_argument('-f','--freqs', help='Spin-frequencies list', required=True, nargs='+')
    parser.add_argument('-a','--accn', help='Acceleration list', required=True, nargs='+')
    parser.add_argument('-s','--sn', help='Required signal-to-noise', required=True, type=float)
    parser.add_argument('-l','--location', help='Path to directory to store filterbanks', required=True, type=str)
    parser.add_argument('-b','--boxcar', help='Input profiles are boxcars - not Gaussians', required=False, action='store_true')
    parser.add_argument('-i','--smear', help='Allow intra-channel smearing', required=False, action='store_true')
    parser.add_argument('-g','--nogen', help='Just show commands - do not generate (debug)', required=False, action='store_true')
    args = parser.parse_args()
    
    if args.boxcar:
        generator = BatchGenerate(args.noise, args.profiles, args.dm, args.freqs, args.accn, args.sn, args.location, args.boxcar, args.nogen, args.smear)
    else:
        generator = BatchGenerate(args.noise, args.profiles, args.dm, args.freqs, args.accn, args.sn, args.location, args.nogen, args.smear)

    generator.make_batch()


if __name__ == '__main__':
    main()
