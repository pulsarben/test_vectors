
# Import standard libraries
import os
import pickle
import pytest
import numpy as np
import sys

# Import test module

from fluxCalc import GetFlux

# Set up test inputs

# Arguments for fluxcalc occur in the following order
sig = None
bw = 320e6
tsys = 25
gain = 1
tobs = 600.0
npol = 1
width = None
period = None

def testcase1():
    flux_calc = GetFlux(100, bw, tsys, gain, tobs, npol, 0.1, 0.5)
    expected_result = 0.0019018144357818268
    assert flux_calc.flux() == expected_result

def testcase2():
    flux_calc = GetFlux(1000, bw, tsys, gain, tobs, npol, 0.1, 0.5)
    expected_result = 0.019018144357818272 
    assert flux_calc.flux() == expected_result

def testcase3():
    flux_calc = GetFlux(100, bw, tsys, gain, tobs, npol, 0.5, 0.5)
    expected_result = 0.00570544330734548
    assert flux_calc.flux() == expected_result

def testcase4():
    flux_calc = GetFlux(100, bw, tsys, gain, tobs, npol, 0.5, 0.01)
    expected_result = 0.00570544330734548
    assert flux_calc.flux() == expected_result

def testcase5():
    flux_calc = GetFlux(100, bw, tsys, gain, 60, npol, 0.5, 0.01)
    expected_result = 0.018042195912175804
    assert flux_calc.flux() == expected_result
