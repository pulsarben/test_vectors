#!/bin/bash


MATLAB_DIR="/raid/TestVectors/docker_scratch/bshaw/PI3/MatLab_Code"

for filterbank_file in `ls | grep fil` ; do

    rm ${MATLAB_DIR}/RCPT/testData.fil 
    echo "working on $filterbank_file"

    # Set up identifier for new files
    ident="${filterbank_file%.*}"

    # Get period in ms from filterbank filename
    period_ms=`echo $filterbank_file | awk '{split($0,a,"_"); print a[3]}'`

    # Convert period to s
    period_s=`echo "$period_ms / 1000" | bc -l | awk '{printf "%f", $0}'`

    # Get DM from filterbank filename
    dm=`echo $filterbank_file | awk '{split($0,a,"_"); print a[5]}'`

    # Create candlist string
    cand_str=`echo "100 $period_s $dm 0 1"`

    # Write candlist string to candlist.txt
    echo $cand_str > ${MATLAB_DIR}/candlist.txt
    echo "candlist looks like this"
    cat ${MATLAB_DIR}/candlist.txt

    # Softlink filterbank file to testData.fil
    echo "Linking ${PWD}/${filterbank_file} to ${MATLAB_DIR}/RCPT/testData.fil" 
    ln -sf ${PWD}/${filterbank_file} ${MATLAB_DIR}/RCPT/testData.fil

    # Run validator
    echo "Running validator"
    matlab -nojvm -nodesktop -nodisplay -r "run ${MATLAB_DIR}/pipeline.m" > ${ident}.txt

    # Grab opt profile
    echo "Copying folded profile"
    cp ${MATLAB_DIR}/FLDO/output/ftscr.1.opt ${ident}.opt

    echo "Done!"

done
