
# Import modules
from __future__ import print_function
import sys
import os
import argparse
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel


class MakeGaussianProfile(object):

    def __init__(self, nbins, dc, show = False, norm = False):

        self.nbins        = nbins # Number of bins across profile
        self.dc           = dc    # Duty cycle
        self.show         = show  # Display plots
        self.norm         = norm  # Normalise to peak
        self.sigma        = None  # Standard deviation of pulse
        self.power        = None  # Power series
        self.fw_bins      = None  # FWHP (in bins)
        self.x            = None  # Bins


    def check_inputs(self):

        outcome = True
        if self.check_dc_val(self.dc):
            outcome = True
        else:
            print("\n======FAILURE=======")
            print("Invalid value for -d. Duty cycle must strictly be >0 and <1")
            outcome = False
            return outcome

        return outcome

    @staticmethod
    def check_dc_val(dc):
        if dc >= 1.0:
            return False
        if dc == 0.0:
            return False
        return True
     
    @staticmethod
    def dc_to_fwhp_bins(nbins, dc):
        fwhp_bins = nbins * dc
        return fwhp_bins

    @staticmethod
    def fwhp_to_sigma(fw_bins):
        sigma = fw_bins / (2.0 * np.sqrt(2*np.log(2.0))) 
        return sigma

    def plot(self):
        plt.plot(self.x, self.power, 'k-')
        plt.xlabel("Bin", fontsize=15)
        plt.ylabel("Probability Density", fontsize=15)
        plt.title("FWHP: {}, Sigma: {}".format(self.fw_bins, self.sigma))
        plt.axhline(np.max(self.power) / 2.0)
        plt.grid()
        plt.tight_layout()
        plt.show()

    def write_out(self, filename):
        if os.path.isfile(filename):
            print("=========FAILURE=======")
            print("File {} already exists - cannot proceed".format(filename))
            sys.exit(9)

        print("Writing file {}".format(filename))
        for i in range(0, len(self.power)):
            with open(filename, 'a') as fout:
                fout.write(str(self.power[i]) + "\n")
        fout.close()

    def do_least_sq(self, power, x):
        model = GaussianModel()
        params = model.guess(power, x=x)
        result = model.fit(power, params, x=x)
        return result

    def filecheck(self, data):
        
        outcome = True
        if len(data) != self.nbins:
            outcome = False
        return outcome

    def generate_profile(self):
        # Update class variables
        if self.check_inputs():
            print("Inputs checked - can proceed")

            # Define sampling
            self.x = np.linspace(0, self.nbins - 1, self.nbins)

            # Define centre position of Gaussian
            centre = self.nbins / 2.0

            # Calculate the number of bins spanning the half-power
            self.fw_bins = self.dc_to_fwhp_bins(self.nbins, self.dc)

            # Calculate sigma
            self.sigma = self.fwhp_to_sigma(self.fw_bins)

            # Generate Gaussian values
            self.power = stats.norm.pdf(self.x, centre, self.sigma)

            # Normalise if requested
            if self.norm:
                self.power = self.power / np.max(self.power)

            # Show diagnostic plot if requested
            if self.show:
                print("Program will continue when plot is closed....")
                self.plot()
            
            # Define output filename and do write
            filename = "Gaussian_DC={}_BINS={}_FWHM_{}.asc".format(self.dc, self.nbins, self.fw_bins)
            self.write_out(filename)

            # Do a least-squares fit on the generated data and 
            # check the parameters make sense
            result = self.do_least_sq(self.power, self.x)

            # Print comparison between demanded and computed values
            print("======Results=======")
            print("Demanded duty cycle: {}".format(self.dc))
            print("Demanded FWHP: {} bins. Actual FWHP: {} bins".format(self.fw_bins, result.params['fwhm'].value))
            print("Demanded stdev: {}. Actual stdev: {}".format(self.sigma, result.params['sigma'].value))
            print("Demanded mean: {}. Actual mean: {}".format(centre, result.params['center'].value))
            if self.norm:
                print("Demanded height: 1.0. Actual height: {}".format(result.params['height'].value))
            else:
                print("Height: {}".format(result.params['height'].value))

            # Check file contains what it should
            final_power = np.loadtxt(filename, unpack=True)
            if not self.filecheck(final_power):
                print("Generated file doesn't look right...")
            else:
                print("File generation complete")

            return result

def main():

    # Process command line args
    parser = argparse.ArgumentParser(description='Gaussian pulse profile generator')
    parser.add_argument('-n', '--nbins', help='Number of bins across pulse profile', required=True, type=int)
    parser.add_argument('-d', '--dc', help='Duty cycle of pulse (as a fraction)', required=True, type=float)
    parser.add_argument('-s', '--show', help='Show diagnostic plots', action='store_true')
    parser.add_argument('-p', '--norm', help='Normalise to peak', action='store_true')
    args = parser.parse_args()

    profile = MakeGaussianProfile(args.nbins, args.dc, args.show, args.norm)
    profile.generate_profile()


if __name__ == '__main__':
    main()
